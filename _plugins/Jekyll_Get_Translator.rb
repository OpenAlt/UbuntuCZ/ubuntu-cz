require 'uri'
require 'net/http'
require 'cgi'
require 'json'

module Jekyll_Translator

  class StringTranslator
    def self.translate(stringToTranslate, sourceLang = "EN", targetLang = "CS")
      if !ENV["deeplApiKey"]
        return stringToTranslate # Ignore if no api key is present
      end

      uri = URI.parse("https://api-free.deepl.com/v2/translate")
      headers = {
          "Authorization" => "DeepL-Auth-Key " + ENV["deeplApiKey"],
          "Content-Type" => "application/json"
      }
      body = {
          "text" => [
            stringToTranslate
          ],
          "source_lang" => sourceLang,
          "target_lang" => targetLang
      }

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      response = http.post(uri.path, body.to_json, headers)

      if response.code == "200"
        # Only replace with translated string if we actually receive any
        data = JSON.parse(response.body)
  
        texts = data["translations"].map do |translation|
          return translation["text"]
        end

        # Ignore any additional texts if there are any, just take the first one
        return texts.size > 0 ? texts.first : stringToTranslate
      end

      return stringToTranslate # Fallback, not translating anything
    end
  end

end
