"use strict";

class TranslationService {
	constructor(data) {
		data = data || {};
		this.url = data.url || "https://iubuntu.cz/translator/translate.php";
		this.fallback = data.fallback || null;
		this.forceFallback = data.forceFallback || false;
		this.credits = null;

		this.cachedReplies = {};
	}

	fetch(string) {
		return fetch(`${this.url}?string=${encodeURIComponent(string)}`, {
			method: "GET",
			mode: "cors",
			referrer: "https://www.ubuntu.cz",
			headers: {
				"Content-type": "application/x-www-form-urlencoded"
			}
		}).then(response => response.text());
	}

	fallbackTranslate(string) {
		console.log("fallbackTranslate");
		return new Promise((resolve, reject) => {
			if(this.fallBackTranslator) {
				this.fallBackTranslator.translate(string).then((data) => resolve(data)).catch((error) => reject(error));
			}
			else if(this.fallback) {
				this.scriptTag = this.scriptTag || document.createElement("script");
				this.scriptTag.src = this.fallback;
				document.head.appendChild(this.scriptTag);

				this.scriptTag.addEventListener("load", (event) => {
					this.fallBackTranslator = new ENCZTranslator({
						capitalizeFirst: true
					});
					resolve(this.fallBackTranslator.translate(string));
				});
			}
		});
	}

	translate(string, forceFetch, forceFallback) {
		return new Promise((resolve, reject) => {
			if(this.forceFallback || forceFallback) {
				this.fallbackTranslate(string).then((data) => resolve(data)).catch((error) => reject(error));
				return;
			}

			if(!forceFetch && this.cachedReplies[string]) {
				resolve({
					text: this.cachedReplies[string].text,
					lang: this.cachedReplies[string].lang,
					cached: true,
					credits: this.credits
				});
				return;
			}

			this.fetch(string).then((text) => {
				if(text) {
					this.cachedReplies[string] = {
						text
					};
					resolve({
						text,
						cached: false,
						credits: this.credits
					});
				}
				else {
					console.error("[TranslationService] Couldn't fetch translation for: " + string);

					if(this.fallBackTranslator) {
						this.fallbackTranslate(string).then((data) => resolve(data)).catch((error) => reject(error));
					}
					else if(this.fallback) {
						this.fallbackTranslate(string).then((data) => resolve(data)).catch((error) => reject(error));
					}
					else {
						reject(false);
					}
				}
			});
		});
	}
}
